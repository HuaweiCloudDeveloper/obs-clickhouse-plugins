### 背景

ClickHouse是一个面向联机分析处理(OLAP)的开源的面向列式存储的DBMS，简称CK, 支持线性扩展，简单方便，高可靠性。与Hadoop, Spark相比，ClickHouse很轻量级,由俄罗斯第一大搜索引擎Yandex于2016年6月发布, 开发语言为C++ 。

目前已经支持一些对象存储的基类，派生类包括S3、Azure Blob Storage和HDFS。

#### 目的

将obs作为clickhouse的存储，备份，可以探索的方向包含但不限于，使用插件方式集成OBS，贡献代码到ClickHouse支持OBS存储，并且提供功能文档，使用文档，示例代码等

#### 参考资料

- [OBS](https://support.huaweicloud.com/bestpractice-obs/obs_05_1507.html)
- [Hdfs支持](https://github.com/jaykelin/clickhouse-hdfs-loader)
- [ClickHouse hdfs支持](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fjaykelin%2Fclickhouse-hdfs-loader)  
- [ClickHouse](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2FClickHouse%2FClickHouse)
- [ClickHouse Backup](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2FAlexAkulov%2Fclickhouse-backup)

